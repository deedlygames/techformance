﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseAnimationSpeed : MonoBehaviour {

    public float acceleration;
    public float animationSpeed = 1;
    public float maxAnimSpeed = 2;

    public float PauseUntilStart = 1;

    public GameObject nextEvent;

    Animator anim;

	// Use this for initialization
	void Start () {
        if (maxAnimSpeed < animationSpeed)
            maxAnimSpeed = animationSpeed;
        anim = GetComponent<Animator>();
        StartCoroutine(StartTimer());
	}

    IEnumerator StartTimer()
    {
        anim.speed = 0;
        yield return new WaitForSeconds(PauseUntilStart);
        StartCoroutine(IncreaseSpeed());
    }
	
	IEnumerator IncreaseSpeed()
    {
        while (animationSpeed < maxAnimSpeed)
        {
            yield return new WaitForSecondsRealtime(.1f);
            animationSpeed += acceleration;
            anim.speed = animationSpeed;
        }

        if (nextEvent != null && animationSpeed >= maxAnimSpeed)
        {
            nextEvent.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
