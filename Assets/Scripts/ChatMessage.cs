﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class ChatMessage
{
    [TextArea(2, 20)]
    public string Message;

    public string Sender;
    public float Timer;

    public ChatMessage (string _Message, string _Sender, float _Timer)
    {
        Message = _Message;
        Sender = _Sender;
        Timer = _Timer;
    }
}
