﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoteButton : MonoBehaviour {

    public enum voteType { Yes, No }
    public voteType Vote;

	public void CastVote()
    {
        if (Vote == voteType.Yes)
            CurrentVotes.votes.Yes++;
        else if (Vote == voteType.No)
            CurrentVotes.votes.No++;
    }
}
