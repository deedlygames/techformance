﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class ChatParticipant {

    public string Name;
    public Sprite Icon;
    public bool isPlayer;
    public Color MessageColor = Color.white;
    public Color TextColor = Color.black;

    public ChatParticipant(string _Name, Sprite _Icon, bool _isPlayer, Color _MessageColor, Color _TextColor)
    {
        Name = _Name;
        Icon = _Icon;
        isPlayer = _isPlayer;
        MessageColor = _MessageColor;
        TextColor = _TextColor;
    }
}
