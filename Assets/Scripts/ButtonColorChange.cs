﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ButtonColorChange : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler {

    public Color pressedColor = Color.white;
    public Color orgColor;
    public float transitionTime = 0.07f;
    float StartTime;

    bool starttimeSet = false;
    bool orgColorSet;

    bool PressCommited = false;

    //[HideInInspector]
    public bool ColorChange = false;

	// Use this for initialization
	void Start () {
        if (GetComponent<Image>().color != orgColor)
        {
            orgColor = GetComponent<Image>().color;
            orgColorSet = true;
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (!starttimeSet)
        {
            StartTime = Time.time;
            starttimeSet = true;
        }

        float timePassed = Time.time - StartTime;
        if (timePassed > transitionTime)
            timePassed = transitionTime;
        float progressTime = timePassed / transitionTime;
        
        if (ColorChange)
            GetComponent<Image>().color = Color.Lerp(orgColor, pressedColor, progressTime);
        else
            GetComponent<Image>().color = Color.Lerp(pressedColor, orgColor, progressTime);

    }

    public void ChangeColor()
    {
        ColorChange = true;
    }   

    private void OnEnable()
    {
        if (orgColorSet)
        {
            ColorChange = false;
            starttimeSet = false;
            GetComponent<Image>().color = orgColor;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        ColorChange = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (ColorChange && !PressCommited)
        {
            ColorChange = false;
            starttimeSet = false;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        PressCommited = true;
    }
}
