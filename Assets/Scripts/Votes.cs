﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Votes {

    public int Yes;
    public int No;

    public Votes (int _Yes, int _No)
    {
        Yes = _Yes;
        No = _No;
    }
}

public static class CurrentVotes {
    private static Votes _votes = new Votes(0,0);
    public static Votes votes
    {
        get { return _votes; }
        set { _votes = value; }
    }
}
