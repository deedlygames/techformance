﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoteManager : MonoBehaviour {

    public Votes currentVotes;

    private void Update()
    {
        currentVotes = CurrentVotes.votes;
    }

    public void sendVotes()
    {
        //Enter code for sending votes here
        print("Yes: " + CurrentVotes.votes.Yes + ", No: " + CurrentVotes.votes.No);

        CurrentVotes.votes = new Votes(0, 0);
    }
}
