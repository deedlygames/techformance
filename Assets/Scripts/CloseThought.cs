﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseThought : MonoBehaviour {

    GameObject thoughtParent;
    Animator anim;

	// Use this for initialization
	void Start () {
        if (transform.parent.GetComponent<Animator>() != null)
            anim = transform.parent.GetComponent<Animator>();

        thoughtParent = transform.parent.parent.parent.gameObject;
	}
	
	public void setThoughtActive(bool isActive)
    {
        
        if (!isActive)
        {
            if (thoughtParent.GetComponent<Chat>() != null)
            {
                
                if (anim != null)
                    StartCoroutine(closeAnimation());
                else
                    thoughtParent.GetComponent<Chat>().close();
                    
            }
            else
                thoughtParent.SetActive(false);

        }
        else
            thoughtParent.SetActive(true);
    }
    
    IEnumerator closeAnimation()
    {
        anim.SetTrigger("Close");
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
        thoughtParent.GetComponent<Chat>().close();
    }
    
}
