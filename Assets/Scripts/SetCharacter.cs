﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCharacter : MonoBehaviour {

    public CharacterManager.character playerChar;

    public void setChar()
    {
        CharacterManager.playerChar = playerChar;
    }
}
