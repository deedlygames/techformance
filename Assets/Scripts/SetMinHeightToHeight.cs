﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetMinHeightToHeight : MonoBehaviour {

    void Awake()
    {
        GetComponent<ContentSizeFitter>().enabled = false;
        GetComponent<LayoutElement>().enabled = false;
    }

	// Use this for initialization
	void Start () {
        float height = GetComponent<RectTransform>().rect.height;

        GetComponent<LayoutElement>().enabled = true;
        GetComponent<LayoutElement>().minHeight = height;

        GetComponent<ContentSizeFitter>().enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
