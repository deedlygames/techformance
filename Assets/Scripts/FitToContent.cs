﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitToContent : MonoBehaviour {

	void Start () {

        StartCoroutine(resizeDelay());
    }

    public IEnumerator resizeDelay()
    {
        yield return new WaitForEndOfFrame();

        resize();
    }
	
    public void resize()
    {
        float newHeight = 0;

        foreach (Transform child in transform)
        {
            if (child.name != "IconContainer")
                newHeight += child.GetComponent<RectTransform>().rect.height;
        }

        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, newHeight);
    }
}
