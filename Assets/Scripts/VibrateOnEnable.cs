﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrateOnEnable : MonoBehaviour {

    float VibrateTime = .2f;
    static bool vibration;

    // Use this for initialization
    private void OnEnable()
    {
        if (vibration) { }
        else
            StartCoroutine(Vibrate());
    }

    private IEnumerator Vibrate()
    {
        vibration = true;      
        yield return new WaitForSeconds(VibrateTime);
        vibration = false;
    }

    private void Update()
    {
        if (vibration)
            Handheld.Vibrate();
    }
}
