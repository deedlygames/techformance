﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetScene : MonoBehaviour
{

    public void resetScene()
    {
        SceneManager.LoadScene(0);
        CurrentVotes.votes = new Votes(0, 0);
    }
}
