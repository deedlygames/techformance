﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour {

    public List<GameObject> Events;

    int eventIndex = 0;

	void Start () {
        closeEvents();
        if (eventIndex < Events.Count)
            Events[eventIndex].SetActive(true);
	}

    void closeEvents()
    {
        foreach (GameObject Event in Events)
        {
            if (eventIndex > 0)
            {
                if (Event == Events[eventIndex+1] || Event.name == "VoteManager")
                { }
                else
                    Event.SetActive(false);
            }
        }
    }
	
	public void startNextEvent () {
        closeEvents();

        eventIndex++;

        if (eventIndex < Events.Count)
            checkCharacter(Events[eventIndex]);
    }

    public void startEventIndex(int index)
    {
        closeEvents();

        eventIndex = index;
        if (eventIndex < Events.Count)
            checkCharacter(Events[eventIndex]);
    }

    void checkCharacter(GameObject currentEvent)
    {
        print("EventIndex: " + eventIndex);

        if (currentEvent.name == "VoteManager")
        {
            currentEvent.GetComponent<VoteManager>().sendVotes();
        }
        else if (currentEvent.GetComponent<EventCharacter>().eventCharacter == CharacterManager.playerChar || currentEvent.GetComponent<EventCharacter>().eventCharacter == CharacterManager.character.all)
        {
            if (currentEvent.activeInHierarchy != true)
            {
                currentEvent.SetActive(true);
                foreach (Transform child in currentEvent.transform)
                {
                    if (child.name != "DontStartOnEnable")
                    {
                        if (child.gameObject.GetComponent<EventCharacter>().eventCharacter == CharacterManager.playerChar || child.gameObject.GetComponent<EventCharacter>().eventCharacter == CharacterManager.character.all)
                            child.gameObject.SetActive(true);
                        else
                            child.gameObject.SetActive(false);
                    }
                }
            }
            else if (currentEvent.GetComponentInChildren<Chat>() != null)
            {
                foreach(Transform child in currentEvent.transform)
                {
                    if (child.gameObject.activeInHierarchy)
                        child.GetComponent<Chat>().NextMessage();
                }
            }
        }
        else
        {
            currentEvent.SetActive(false);
        }
    }
}
