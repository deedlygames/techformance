﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chat : MonoBehaviour {

    public enum MessageTypeEnum { Chat, Thought, Vote };
    public MessageTypeEnum MessageType;

    public GameObject MessagePrefab;
    public GameObject MessageSpawner;
    public GameObject MessageScreen;

    public float MessageSpacing;
    float MessageHeight = 0;

    public bool UseAutomaticTiming = true;
    float AutoTimer = 0.24f;
    float AutoTimerBreak = 2;
    float TextSpeedMultiplier = 1.5f;

    public List<ChatParticipant> chatParticipants;
    public List<ChatMessage> chatMessage;

    string lastSender;

    public GameObject NextPart;

    //[HideInInspector]
    public GameObject StartingObject;

    int currentMessageIndex = 0;

    public bool ContinueStarterObject = false;

    // Use this for initialization
    void Start () {

        if (MessageType == MessageTypeEnum.Vote)
        {
            CurrentVotes.votes = new Votes(0, 0);
        }

        NextMessage();

    }
	
	public void NextMessage ()
    {
        if (chatMessage.Count > currentMessageIndex)
        {
            if (MessageType == MessageTypeEnum.Chat)
            {
                if (chatMessage[currentMessageIndex].Message.Contains("{!}"))
                {
                    string thoughtName = chatMessage[currentMessageIndex].Message.Replace("{!}", "");

                    GameObject newThought = transform.parent.parent.Find(thoughtName).gameObject;

                    newThought.SetActive(true);
                    if (newThought.GetComponent<Chat>() != null)
                    {
                        newThought.GetComponent<Chat>().StartingObject = gameObject;
                    }

                    currentMessageIndex++;
                }
                else if (
                    chatMessage[currentMessageIndex].Message.Contains("{Yes}") && CurrentVotes.votes.Yes >= CurrentVotes.votes.No ||
                    chatMessage[currentMessageIndex].Message.Contains("{No}") && CurrentVotes.votes.No > CurrentVotes.votes.Yes ||
                    !chatMessage[currentMessageIndex].Message.Contains("{Yes}") && !chatMessage[currentMessageIndex].Message.Contains("{No}")
                )
                {
                    string newMessage = chatMessage[currentMessageIndex].Message.Replace("{Yes}", "");
                    newMessage = newMessage.Replace("{No}", "");

                    GameObject NewMessage = Instantiate(MessagePrefab, MessageSpawner.transform);

                    NewMessage.transform.Find("Message").Find("MessageText").GetComponentInChildren<Text>().text = newMessage;
                    NewMessage.transform.Find("Sender").GetComponentInChildren<Text>().text = chatMessage[currentMessageIndex].Sender;

                    int SenderIndex = 0;

                    for (int i = 0; i < chatParticipants.Count; i++)
                    {
                        if (chatMessage[currentMessageIndex].Sender == chatParticipants[i].Name)
                        {
                            SenderIndex = i;
                        }
                    }

                    NewMessage.transform.Find("Message").GetComponent<Image>().color = chatParticipants[SenderIndex].MessageColor;
                    NewMessage.transform.Find("Message").Find("MessageText").GetComponent<Text>().color = chatParticipants[SenderIndex].TextColor;
                    NewMessage.transform.Find("IconContainer").GetComponent<Image>().color = chatParticipants[SenderIndex].MessageColor;
                    if (chatParticipants[SenderIndex].Icon != null)
                        NewMessage.transform.Find("IconContainer").Find("Icon").GetComponent<Image>().sprite = chatParticipants[SenderIndex].Icon;

                    if (chatParticipants[SenderIndex].isPlayer)
                    {
                        NewMessage.transform.Find("Message").Find("MessageText").GetComponentInChildren<Text>().alignment = TextAnchor.MiddleRight;
                        NewMessage.transform.Find("Sender").GetComponentInChildren<Text>().alignment = TextAnchor.LowerRight;

                        NewMessage.transform.Find("IconContainer").GetComponent<RectTransform>().pivot = new Vector2(0, 1);
                        NewMessage.transform.Find("IconContainer").GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
                        NewMessage.transform.Find("IconContainer").GetComponent<RectTransform>().anchorMin = new Vector2(1, 1);
                        NewMessage.transform.Find("IconContainer").GetComponent<RectTransform>().anchoredPosition = new Vector2(10, 0);
                    }
                    else
                    {
                        NewMessage.transform.Find("Message").Find("MessageText").GetComponentInChildren<Text>().alignment = TextAnchor.MiddleLeft;
                        NewMessage.transform.Find("Sender").GetComponentInChildren<Text>().alignment = TextAnchor.LowerLeft;

                        NewMessage.transform.Find("IconContainer").GetComponent<RectTransform>().pivot = new Vector2(1, 1);
                        NewMessage.transform.Find("IconContainer").GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
                        NewMessage.transform.Find("IconContainer").GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
                        NewMessage.transform.Find("IconContainer").GetComponent<RectTransform>().anchoredPosition = new Vector2(-10, 0);
                    }

                    if (lastSender == chatMessage[currentMessageIndex].Sender)
                    {
                        Destroy(NewMessage.transform.Find("Sender").gameObject);
                        Destroy(NewMessage.transform.Find("IconContainer").gameObject);
                        //StartCoroutine(NewMessage.GetComponent<FitToContent>().resizeDelay());
                    }

                    

                    //NewMessage.GetComponent<RectTransform>().localPosition = new Vector3(NewMessage.GetComponent<RectTransform>().localPosition.x, MessageHeight, NewMessage.GetComponent<RectTransform>().localPosition.y);

                    startLayoutElements(NewMessage);

                    lastSender = chatMessage[currentMessageIndex].Sender;

                    currentMessageIndex++;

                    if (chatMessage.Count > currentMessageIndex)
                    {
                        while (chatMessage[currentMessageIndex].Message.Contains("{No}") && CurrentVotes.votes.Yes >= CurrentVotes.votes.No ||
                        chatMessage[currentMessageIndex].Message.Contains("{Yes}") && CurrentVotes.votes.No > CurrentVotes.votes.Yes ||
                        chatMessage.Count <= currentMessageIndex)
                            currentMessageIndex++;

                        StartCoroutine(MessTimer());
                    }
                        
                }
                else
                {
                    currentMessageIndex++;

                    if (chatMessage.Count > currentMessageIndex)
                    {
                        while (chatMessage[currentMessageIndex].Message.Contains("{No}") && CurrentVotes.votes.Yes >= CurrentVotes.votes.No ||
                        chatMessage[currentMessageIndex].Message.Contains("{Yes}") && CurrentVotes.votes.No > CurrentVotes.votes.Yes ||
                        chatMessage.Count <= currentMessageIndex)
                            currentMessageIndex++;

                        StartCoroutine(MessTimer());
                    }
                }

            }
            else if (MessageType == MessageTypeEnum.Thought)
            {
                foreach (Transform child in MessageSpawner.transform)
                {
                    if (child.name != "Character")
                    Destroy(child.gameObject);
                }

                GameObject NewMessage = Instantiate(MessagePrefab, MessageSpawner.transform);

                NewMessage.GetComponentInChildren<Text>().text = chatMessage[0].Message;

                NewMessage.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

                NewMessage.GetComponent<Image>().color = chatParticipants[0].MessageColor;
                NewMessage.GetComponentInChildren<Text>().color = chatParticipants[0].TextColor;

                currentMessageIndex++;



                if (chatMessage.Count > currentMessageIndex)
                    StartCoroutine(MessTimer());
            }
            
        }

        else
            StartNextPart();

    }

    public void close()
    {
        if (StartingObject != null && ContinueStarterObject)
            StartingObject.GetComponent<Chat>().startMessTimer();

        transform.gameObject.SetActive(false);
    }

    public void StartNextPart()
    {
        if (NextPart != null)
        {
            NextPart.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    public void startMessTimer()
    {
        StartCoroutine(MessTimer());
    }

    IEnumerator MessTimer()
    {
        if (!UseAutomaticTiming)
            yield return new WaitForSeconds(chatMessage[currentMessageIndex].Timer);
        else {
            int wordcount = chatMessage[currentMessageIndex - 1].Message.Split(' ').Length - 1;
            yield return new WaitForSeconds(((float)wordcount * AutoTimer + AutoTimerBreak) * TextSpeedMultiplier);
        }
        NextMessage();
    }

    void startLayoutElements(GameObject NewMessage)
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)NewMessage.transform);
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)MessageSpawner.transform);
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)NewMessage.transform.Find("Message").transform);       
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)NewMessage.transform.Find("Message").Find("MessageText").transform);
        StartCoroutine(resizeDelay(NewMessage));
        //resize(NewMessage);
    }

    public IEnumerator resizeDelay(GameObject NewMessage)
    {
        yield return new WaitForEndOfFrame();
        resize(NewMessage);
    }

    void resize(GameObject NewMessage)
    {
        NewMessage.GetComponent<FitToContent>().resize();
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)NewMessage.transform);
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)MessageSpawner.transform);
    }
}
